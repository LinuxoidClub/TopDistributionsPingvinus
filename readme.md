Для захода по ссылкам с Украины требуется VPN.

| Номер | Название                                                             | Рейтинг (pingvinus.ru) | Комментарий                                                                          |
|-------|----------------------------------------------------------------------|------------------------|--------------------------------------------------------------------------------------|
| 1     | [Manjaro](https://pingvinus.ru/distribution/manjaro)                 | 96.6                   | Часто ломается, не рекомендуется нами                                            |
| 2     | [Linux Mint](https://pingvinus.ru/distribution/linuxmint)            | 66.7                   | Хороший выбор для новичков                                                           |
| 3     | [Arch Linux](https://pingvinus.ru/distribution/arch-linux)           | 58.3                   | Хороший выбор для опытных пользователей                                              |
| 4     | [Debian](https://pingvinus.ru/distribution/debian)                   | 45.5                   | Хороший выбор для опытных, но не прихотливых                                         |
| 5     | [Q4OS](https://pingvinus.ru/distribution/q4os)                       | 33.6                   | Идёт как очень похожая на Windows система, а винда хуйня                                            |
| 6     | [ArcoLinux](https://pingvinus.ru/distribution/arcolinux)             | 32.9                   | Уже установленный арч линукс                                                         |
| 7     | [Elementary OS](https://pingvinus.ru/distribution/elementary-os)     | 32.1                   | Напоминает macOS. Подходит для новичков.                                             |
| 8     | [MX Linux](https://pingvinus.ru/distribution/mx-linux)               | 28.6                   | Дебиан-бейсед                                                                        |
| 10    | [Deepin Linux](https://pingvinus.ru/distribution/deepin-linux)       | 26.6                   | Если вы его поставить, великий Китай начислит вам +15 социал кредит, один кошка жена и миска риса                                                                 |
| 11    | [OpenSUSE](https://pingvinus.ru/distribution/opensuse)               | 25.6                   | Почти клон арча                                           |
| 12    | [Artix Linux](https://pingvinus.ru/distribution/artix)               | 23.5                   | Non-systemd Arch Linux                                                               |
| 13    | [Solus](https://pingvinus.ru/distribution/solus)                     | 23.5                   | Интересный, похожий по дизайну на винду дистрибутив                                                          |
| 14    | [Fedora](https://pingvinus.ru/distribution/fedora)                   | 19.6                   | ДИСТРИБУТИВ КОРПОРАЦИИ RED HAT. А ЗНАЧИТ ХРЕНЬ                                       |
| 15    | [Vanilla OS](https://pingvinus.ru/distribution/vanilla-os)           | 19.4                   | Сильно нацелена на безопасность.                                                     |
| 16    | [SliTaz](https://pingvinus.ru/distribution/slitaz)                   | 19.2                   | Для компьютеров с очень малым количеством ОЗУ                                        |
| 17    | [Pop!_OS](https://pingvinus.ru/distribution/popos)                   | 18.4                   | Ещё больше обдристанная убунту                   |
| 18    | [ROSA Fresh](https://pingvinus.ru/distribution/rosa-fresh)           | 18.4                   | Российский дистрибутив, а значит хуйня.                                              |
| 19    | [antiX](https://pingvinus.ru/distribution/antix)                     | 18.1                   | Дистр для слабых компьютеров.                                                        |
| 20    | [Clear Linux](https://pingvinus.ru/distribution/clear-linux)         | 17.9                   | Оптимизированный дистр от интел                                                      |
| 21    | [SparkyLinux](https://pingvinus.ru/distribution/sparkylinux)         | 17.9                   | Дебиан-бейсед                                                                        |
| 22    | [Puppy Linux](https://pingvinus.ru/distribution/puppy-linux)         | 17.2                   | Самостоятельный дистрибутив, нацелен на слабые ПК                                                          |
| 23    | [Void Linux](https://pingvinus.ru/distribution/voidlinux)            | 17                     | Non-systemd. Самостоятельный дистрибутив                                             |
| 24    | [Archman](https://pingvinus.ru/distribution/archman)                 | 16.5                   | Arch-бейсед дистрибутив                                                              |
| 25    | [Kodachi](https://pingvinus.ru/distribution/linux-kodachi)           | 15.6                   | Хакерский дистрибутив. LiveCD                                                        |
| 26    | [Gentoo](https://pingvinus.ru/distribution/gentoo)                   | 14.9                   | Для очень продвинутых пользователей. Максимальная скорость, но сложность в установке |
| 27    | [Calculate Linux](https://pingvinus.ru/distribution/calculate-linux) | 14.7                   | Gentoo-бейсед                                                                        |
| 28    | [Peppermint OS](https://pingvinus.ru/distribution/peppermint-os)     | 14.6                   | Ориентирована на веб                                                                 |
| 29    | [Garuda Linux](https://pingvinus.ru/distribution/garuda-linux)       | 14.4                   | "Геймерский" (или лучше геморойный) дистрибутив                                      |
| 30    | [Kali Linux](https://pingvinus.ru/distribution/kali-linux)           | 14                     | Хакерский дистрибутив (ну или типо того лол)                                         |
| 31    | [EndeavourOS](https://pingvinus.ru/distribution/endeavouros)         | 13.7                   | Arch-бейсед, рекомендуется к установке                                                                          |
| 32    | [DamnSmallLinux](https://pingvinus.ru/distribution/damnsmalllinux)   | 13.4                   | Очень маленький дистрибутив. Только в экстренных случаях                             |
| 33    | [PCLinuxOS](https://pingvinus.ru/distribution/pclinuxos)             | 13.2                   | Бумерский дистрибутив                                                                |
| 34    | [KDE neon](https://pingvinus.ru/distribution/kde-neon)               | 13                     | Офицальный дистрибутив от KDE. Хрень, потому что ещё хуже чем Kubuntu                |
| 35    | [Tiny Core Linux](https://pingvinus.ru/distribution/tiny-core-linux) | 12.5                   | Ещё меньше чем DSL. Для экстренных случаев                                           |
| 36    | [NixOS](https://pingvinus.ru/distribution/nixos)                     | 12.2                   | Основан на пакет менеджере Nix, сложный                                                       |
| 37    | [ReactOS](https://pingvinus.ru/distribution/reactos)                 | 12.2                   | Не Linux дистрибутив. Воссоздаёт NT (ядро Windows) с нуля                            |
| 38    | [FreeBSD](https://pingvinus.ru/distribution/freebsd)                 | 12.1                   | BSD дистрибутив. Убунту в мире БСД                                                   |
| 39    | [Linux Lite](https://pingvinus.ru/distribution/linux-lite)           | 11.5                   | Если вам, по каким то причинам, не получилось на минте, то попробуйте это            |
| 40    | [Android x86](https://pingvinus.ru/distribution/android-x86)         | 11.3                   | Андроид)                                                                             |
| 41    | [Endless OS](https://pingvinus.ru/distribution/endless-os)           | 10.7                   | Vannila OS, но только для дебиан                                                     |
| 42    | [CentOS](https://pingvinus.ru/distribution/centos)                   | 9.9                    | ДИСТРИБУТИВ RED HAT. УЖЕ МЁРТВ                                                       |
| 44    | [KaOS](https://pingvinus.ru/distribution/kaos)                       | 9                      | Независимый дистрибутив. Ориентирован польностью на окружение KDE Plasma             |
| 45    | [4MLinux](https://pingvinus.ru/distribution/4mlinux)                 | 8.9                    | Независимый, маленький дистр. Больше для серверов                                    |
| 46    | [Devuan](https://pingvinus.ru/distribution/devuan)                   | 8.8                    | Non-systemd Debian                                                                   |
| 47    | [Bodhi Linux](https://pingvinus.ru/distribution/bodhi-linux)         | 8.5                    | Ubuntu на Moksha                                                                     |
| 48    | [Alpine Linux](https://pingvinus.ru/distribution/alpine-linux)       | 8.5                    | Независимый, non-systemd дистрибутив                                                 |
| 51    | [Dragora](https://pingvinus.ru/distribution/dragora)                 | 8.1                    | Самостоятельный дистрибутив, без проприетарщины и минималистичный                    |
| 52    | [Tails](https://pingvinus.ru/distribution/tails)                     | 7.8                    | Амнестический дистрибутив, созданный для анонимного использования сети.              |
| 53    | [Slackware](https://pingvinus.ru/distribution/slackware)             | 7.7                    | Старичёк, но досихпор поддерживается! Non-systemd.                                   |
| 54    | [OpenMandriva](https://pingvinus.ru/distribution/openmandriva)       | 7.7                    | Спорный, основанный на ROSA, дистрибутив. Не российский.                             |
| 55    | [CrunchBang](https://pingvinus.ru/distribution/crunchbang)           | 7.6                    | Лёгкий дистр, основанный на дебиан.                                                  |
| 56    | [Nitrux](https://pingvinus.ru/distribution/nitrux)                   | 7.5                    | Non-systemd, Debian-based с KDE на борту и XanMod кернелом.                          |
| 57    | [Mageia](https://pingvinus.ru/distribution/mageia)                   | 7.4                    | Возрождение Mandriva                                                                 |
| 58    | [Netrunner](https://pingvinus.ru/distribution/netrunner)             | 7.4                    | С КDE на борту. Имеет поддержку ARM                                                  |
| 59    | [Parrot Linux](https://pingvinus.ru/distribution/parrot-linux)       | 7                      | Хакерский дистрибутив. LiveCD                                                        |
| 60    | [Trisquel](https://pingvinus.ru/distribution/trisquel)               | 6.5                    | Убунту, очищенная от проприетарного                                                  |
| 61    | [Sabayon Linux](https://pingvinus.ru/distribution/sabayon-linux)     | 6                      | Основана на Gentoo                                                                   |
| 62    | [Rocky Linux](https://pingvinus.ru/distribution/rocky-linux)         | 5.4                    | RED HAT!                                                                             |
| 63    | [TrueOS](https://pingvinus.ru/distribution/trueos)                   | 4.4                    | FreeBSD-бейсед                                                                       |
| 64    | [GParted Live](https://pingvinus.ru/distribution/gparted-live)       | 4.2                    | Gparted на LiveCD                                                                    |
| 65    | [AlmaLinux](https://pingvinus.ru/distribution/almalinux)             | 3                      | Свободная CentOS, её продолжение                                                     |